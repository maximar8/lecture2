package sbp.exceptions;

import java.io.PrintWriter;
import java.io.StringWriter;

public class WorkWithExceptions
{
    public static void main(String[] args) throws Exception {
        throwCheckedException();

    }
     /**
     * В данном методе необходимо вызвать методы throwCheckedException и throwUncheckedException.
     * Все исключения должны быть обработаны
     * Необходимо вывести описание exception и stacktrace в консоль
     * Впойманное исключение необходимо упаковать в кастомное исключение и пробросить выше
     * Перед завершением работы метода обязательно необходимо вывести в консоль сообщение "Finish"
     */
    public void exceptionProcessing() throws Exception  {
        try {
            throwCheckedException();
            throwUncheckedException();
        } catch (Exception ex) {
            StringWriter sw = null;
            sw = new StringWriter();
            ex.printStackTrace(new PrintWriter(sw));
            String stackTrace = sw.toString();
            System.out.println(stackTrace);
        }
        System.out.println("Finish");
    }

    /**
     * (* - необязательно) Доп. задание.
     * Выписать в javadoc (здесь) - все варианты оптимизации и устранения недочётов метода
     * Ошибка RuntimeException вызывается в методе 2 раза. Имеет смысл вызывать ошибку лишь один раз
     * @throws IllegalStateException
     * @throws Exception
     * @throws RuntimeException
     */
    public void hardExceptionProcessing() throws IllegalStateException, Exception, RuntimeException
    {
        System.out.println("Start");
        try
        {
            System.out.println("Step 1");
            throw new IllegalArgumentException();
        }
        catch (IllegalArgumentException e)
        {
            System.out.println("Catch IllegalArgumentException");
            throw new RuntimeException("Step 2");
        }
        catch (RuntimeException e)
        {
            System.out.println("Catch RuntimeException");
            throw new RuntimeException("Step 3");
        }
        finally
        {
            System.out.println("Step finally");
            throw new RuntimeException("From finally");
        }
    }

    public void throwCheckedException() throws Exception
    {
        throw new Exception("Checked exception");
    }

    public void throwUncheckedException()
    {
        throw new RuntimeException("Unchecked exception");
    }
}
