package sbp.io;
import java.io.*;
import java.sql.SQLOutput;

public class MyIOExample {

    public MyIOExample(File fileForTest) {

    }

    public static void main(String[] args) throws IOException {
        System.out.println(workWithFile("test0.txt"));
        System.out.println(copyBufferedFile("test1.txt", "test2.txt"));
        System.out.println(copyFile("test1.txt", "test3.txt"));
        System.out.println(copyFileWithReaderAndWriter("test1.txt", "test4.txt"));

    }

     /**
     * Создать объект класса {@link java.io.File}, проверить существование и чем является (фалй или директория).
     * Если сущность существует, то вывести в консоль информацию:
     *      - абсолютный путь
     *      - родительский путь
     * Если сущность является файлом, то вывести в консоль:
     *      - размер
     *      - время последнего изменения
     * Необходимо использовать класс {@link java.io.File}
     * @param fileName - имя файла
     * @return - true, если файл успешно создан
     */
    public static boolean workWithFile(String fileName) throws IOException {
        File file = new File(fileName);
        boolean exists = file.exists();
        boolean isFile = file.isFile();
        boolean isDirectory = file.isDirectory();

        if(exists) {
            String AbsolutePath = file.getAbsolutePath();
            String Parent = file.getParent();
            System.out.println(AbsolutePath);
            System.out.println(Parent);
        }

        if(isFile) {
            long size = file.length();
            long lastModified = file.lastModified();
            System.out.println(size);
            System.out.println(size);
        }

        if(file.createNewFile()) {
            return true;
        } else {
            return false;
        }

    }

    /**
     * Метод должен создавать копию файла
     * Необходимо использовать IO классы {@link java.io.FileInputStream} и {@link java.io.FileOutputStream}
     * @param sourceFileName - имя исходного файла
     * @param destinationFileName - имя копии файла
     * @return - true, если файл успешно скопирован
     */
    public static boolean copyFile(String sourceFileName, String destinationFileName) throws IOException {
        FileInputStream file1 = new FileInputStream(sourceFileName);
        FileOutputStream file2 = new FileOutputStream(destinationFileName);

        int symbol;
        try {
            while ((symbol = file1.read()) != -1) {
                file2.write(symbol);
            }
        } finally {
            file1.close();
            file2.close();
        }

        File file = new File(destinationFileName);

        if(file.exists()) {
            return true;
        } else
        return false;
    }

    /**
     * Метод должен создавать копию файла
     * Необходимо использовать IO классы {@link java.io.BufferedInputStream} и {@link java.io.BufferedOutputStream}
     * @param sourceFileName - имя исходного файла
     * @param destinationFileName - имя копии файла
     * @return - true, если файл успешно скопирован
     */
    public static boolean copyBufferedFile(String sourceFileName, String destinationFileName) throws IOException {
        BufferedReader file1 = new BufferedReader(new FileReader(sourceFileName));
        FileWriter file2 = new FileWriter(destinationFileName);

        String line;
        try {
            while ((line = file1.readLine()) != null) {
                file2.write(line);
            }
        } finally {
            file1.close();
            file2.close();
        }

        File file = new File(destinationFileName);
        if(file.exists()) {
            return true;
        } else
            return false;
    }

    /**
     * Метод должен создавать копию файла
     * Необходимо использовать IO классы {@link java.io.FileReader} и {@link java.io.FileWriter}
     * @param sourceFileName - имя исходного файла
     * @param destinationFileName - имя копии файла
     * @return - true, если файл успешно скопирован
     */
    public static boolean copyFileWithReaderAndWriter(String sourceFileName, String destinationFileName) throws IOException {
        FileReader file1 = new FileReader(sourceFileName);
        FileWriter file2 = new FileWriter(destinationFileName);

        int symbol;
        try {
            while ((symbol = file1.read()) != -1) {
                file2.write(symbol);
            }
        } finally {
            file1.close();
            file2.close();
        }

        File file = new File(destinationFileName);
        if(file.exists()) {
            return true;
        } else
            return false;
    }
}
