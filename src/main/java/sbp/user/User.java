package sbp.user;

/**
 * В классе определено 2 конструктора. Второй отличается от первого наличием поля oldCardDrive
 * Переопределенный метод toString позволяет получить информацию об объекте
 * Метод setOldKBM позволяет присвоить текущее значение КБМ
 * Метод getNewKBM позволяет получить новое значение КБМ
 */
public class User {
    private String name;
    private String birthday;
    private String newCardDrive;
    private String oldCardDrive;
    private double oldKBM;
    private double newKBM;

    public User(String name, String birthday, String newCardDrive, String oldCardDrive) {
        this.name = name;
        this.birthday = birthday;
        this.newCardDrive = newCardDrive;
        this.oldCardDrive = oldCardDrive;
        this.oldKBM = 1;
        this.newKBM = 1;
    }

    public User(String name, String birthday, String newCardDrive) {
        this.name = name;
        this.birthday = birthday;
        this.newCardDrive = newCardDrive;
        this.oldKBM = 1;
        this.newKBM = 1;
    }

    @Override
    public String toString() {
        System.out.println("ФИО: " + name);
        System.out.println("Дата рождения: " +birthday);
        System.out.println("Новое водительское удостоверение: " +newCardDrive);
        System.out.println("Старое водительское удостоверение: " +oldCardDrive);
        System.out.println("Текущий КБМ: " + oldKBM);
        System.out.println("Новый КБМ: " + newKBM);
        return null;
    }


    public void setOldKBM (double oldKBM) {
        this.oldKBM = oldKBM;
    }

    public double getNewKBM() {
        this.newKBM = this.newKBM - 0.1;
        return this.newKBM;
    }

    public String getName() {
        return this.name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User)o;
        return this.name == user.name;
    }

    @Override
    public int hashCode() {
        int i = Integer.parseInt(name);
        return i;
    }



}
