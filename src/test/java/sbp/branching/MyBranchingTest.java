package sbp.branching;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import sbp.common.Utils;

import static org.mockito.ArgumentMatchers.anyBoolean;
import static org.mockito.ArgumentMatchers.anyString;

public class MyBranchingTest {

    /**
     * Тест метода maxInt
     * Проверка возвращаемого результата в зависимости от водных данных i1 и i2
     * Проверка количества обращений к {@link Utils} при возвращаемом значении true метода utilFunc2
     */
    @Test
    public void maxInt_Success_Test() {
        final int i1 = 15;
        final int i2 = 35;

        Utils utilsForTest = Mockito.mock(Utils.class);
        Mockito.when(utilsForTest.utilFunc2()).thenReturn(true);
        MyBranching object1 = new MyBranching(utilsForTest);

        int result = object1.maxInt(i1, i2);
        Assertions.assertEquals(result, 0);
        Mockito.verify(utilsForTest, Mockito.times(i2)).utilFunc1("Hello");
    }

    /**
     * Тест метода maxInt
     * Проверка возвращаемого результата в зависимости от водных данных i1 и i2
     * Проверка количества обращений к {@link Utils} при возвращаемом значении false метода utilFunc2
     */
    @Test
    public void maxInt_Fail_Test() {
        final int i1 = 15;
        final int i2 = 35;

        Utils utilsForTest = Mockito.mock(Utils.class);
        Mockito.when(utilsForTest.utilFunc2()).thenReturn(false);
        MyBranching object1 = new MyBranching(utilsForTest);
        int result = object1.maxInt(i1, i2);
        Assertions.assertEquals(result, Math.max(i1, i2));
        Mockito.verify(utilsForTest, Mockito.times(1)).utilFunc2();
        Mockito.verify(utilsForTest, Mockito.times(0)).utilFunc1("Hello");
    }

    /**
     * Тест метода ifElseExample
     * Проверка возвращаемого значения метода ifElseExample при возвращаемом значении true метода utilFunc2
     */
    @Test
    public void ifElseExample_Success_Test() {
        Utils utilsForTest = Mockito.mock(Utils.class);
        Mockito.when(utilsForTest.utilFunc2()).thenReturn(true);
        MyBranching object = new MyBranching(utilsForTest);

        boolean result = object.ifElseExample();
        Assertions.assertTrue(result);
    }

    /**
     * Тест метода ifElseExample
     * Проверка возвращаемого значения метода ifElseExample при возвращаемом значении false метода utilFunc2
     */
    @Test
    public void ifElseExample_Fail_Test() {
        Utils utilsForTest = Mockito.mock(Utils.class);
        Mockito.when(utilsForTest.utilFunc2()).thenReturn(false);
        MyBranching object = new MyBranching(utilsForTest);

        boolean result = object.ifElseExample();
        Assertions.assertFalse(result);
    }

    /**
     * Тест метода switchExample
     * Проверка колличества обращений к Utils#utilFunc1 и Utils#utilFunc2 при входящем i = 1
     */
    @Test
    public void switchExample_Test1() {
        final int i = 1;

        Utils utilsForTest = Mockito.mock(Utils.class);

        MyBranching object = new MyBranching(utilsForTest);
        object.switchExample(i);
        Mockito.verify(utilsForTest, Mockito.times(1)).utilFunc1("abc");
        Mockito.verify(utilsForTest, Mockito.times(1)).utilFunc2();
    }

    /**
     * Тест метода switchExample
     * Проверка колличества обращений к Utils#utilFunc1 и Utils#utilFunc2 при входящем i = 2
     */
    @Test
    public void switchExample_Test2() {
        final int i = 2;

        Utils utilsForTest = Mockito.mock(Utils.class);
        MyBranching object = new MyBranching(utilsForTest);
        object.switchExample(i);
        Mockito.verify(utilsForTest, Mockito.times(0)).utilFunc1("abc");
        Mockito.verify(utilsForTest, Mockito.times(1)).utilFunc2();
    }

    /**
     * Тест метода switchExample
     * Проверка колличества обращений к Utils#utilFunc1 и Utils#utilFunc2 при входящем i = 0 и вовзаращемом значении false метода utilFunc2()
     */
    @Test
    public void switchExample_Test3False() {
        final int i = 0;

        Utils utilsForTest = Mockito.mock(Utils.class);
        Mockito.when(utilsForTest.utilFunc2()).thenReturn(false);
        MyBranching object = new MyBranching(utilsForTest);
        object.switchExample(i);
        Mockito.verify(utilsForTest, Mockito.times(0)).utilFunc1("abc");
        Mockito.verify(utilsForTest, Mockito.times(1)).utilFunc2();
    }

    /**
     * Тест метода switchExample
     * Проверка колличества обращений к Utils#utilFunc1 и Utils#utilFunc2 при входящем i = 0 и вовзаращемом значении true метода utilFunc2()
     */
    @Test
    public void switchExample_Test3True() {
        final int i = 0;

        Utils utilsForTest = Mockito.mock(Utils.class);
        Mockito.when(utilsForTest.utilFunc2()).thenReturn(true);
        MyBranching object = new MyBranching(utilsForTest);
        object.switchExample(i);
        Mockito.verify(utilsForTest, Mockito.times(1)).utilFunc2();
        Mockito.verify(utilsForTest, Mockito.times(1)).utilFunc1("abc2");
    }


}