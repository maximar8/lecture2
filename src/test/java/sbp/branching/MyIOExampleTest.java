package sbp.branching;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import sbp.io.MyIOExample;
import java.io.File;
import java.io.IOException;

public class MyIOExampleTest {

    /**
     * Тест метода workWithFile
     * Проверка факта создания файла при вводных данных fileName
     */
    @Test
    public void workWithFile() throws IOException {
        String fileName = "test0.txt";
        File fileForTest = Mockito.mock(File.class);
        MyIOExample object1 = new MyIOExample(fileForTest);
        boolean result = object1.workWithFile(fileName);
        Assertions.assertTrue(result);
    }

    /**
     * Тест метода copyFile
     * Проверка факта создания копии файла sourceFileName с названием destinationFileName
     */
    @Test
    public void copyFile() throws IOException {
        String sourceFileName = "test1.txt";
        String destinationFileName = "test2.txt";
        File fileForTest = Mockito.mock(File.class);
        MyIOExample object1 = new MyIOExample(fileForTest);
        boolean result = object1.copyFile(sourceFileName, destinationFileName);
        Assertions.assertTrue(result);
    }

    /**
     * Тест метода copyBufferedFile
     * Проверка факта создания копии файла sourceFileName с названием destinationFileName
     */
    @Test
    public void copyBufferedFile() throws IOException {
        String sourceFileName = "test1.txt";
        String destinationFileName = "test2.txt";
        File fileForTest = Mockito.mock(File.class);
        MyIOExample object1 = new MyIOExample(fileForTest);
        boolean result = object1.copyBufferedFile(sourceFileName, destinationFileName);
        Assertions.assertTrue(result);
    }

    /**
     * Тест метода copyBufferedFile
     * Проверка факта создания копии файла sourceFileName с названием destinationFileName
     */
    @Test
    public void copyFileWithReaderAndWriter() throws IOException {
        String sourceFileName = "test1.txt";
        String destinationFileName = "test2.txt";
        File fileForTest = Mockito.mock(File.class);
        MyIOExample object1 = new MyIOExample(fileForTest);
        boolean result = object1.copyFileWithReaderAndWriter(sourceFileName, destinationFileName);
        Assertions.assertTrue(result);
    }

}
