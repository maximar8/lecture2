package sbp.branching;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import sbp.exceptions.WorkWithExceptions;

import java.io.IOException;

public class WorkWithExceptionsTest {

    /**
     * Тест метода throwCheckedException()
     * Проверяем факт пробрасывания ошибки
     */
    @Test
    public void throwCheckedException() {
        Assertions.assertThrows(java.lang.Exception.class, WorkWithExceptions::throwCheckedException);
    }

    /**
     * Тест метода throwCheckedException()
     * Проверяем факт пробрасывания ошибки
     */
    @Test
    public void throwUncheckedException() {
        Assertions.assertThrows(java.lang.Exception.class, WorkWithExceptions::throwUncheckedException);
    }

    /**
     * Тест метода exceptionProcessing()
     * Проверяем факт пробрасывания ошибок
     */
    @Test
    public void exceptionProcessing() {
        Assertions.assertThrows(java.lang.Exception.class, WorkWithExceptions::throwUncheckedException);
        Assertions.assertThrows(java.lang.Exception.class, WorkWithExceptions::throwCheckedException);
    }



}
